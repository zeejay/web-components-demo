(function (myUtils) {

    myUtils.loadScript = function (src) {
        var script = document.createElement("script");
        script.src = src;

        document.head.appendChild(script);
    };

    myUtils.isHtmlImportSupported = function () {
        return 'import' in document.createElement('link');
    };

    myUtils.isCustomElementSupported = function () {
        return 'registerElement' in document;
    };

    myUtils.isShadowDomSupported = function () {
        return 'createShadowRoot' in document.head;
    };

}(window.myUtils = (window.myUtils || {})));