(function (ce) {

    var proto = Object.create(HTMLButtonElement.prototype);

    proto.createdCallback = function () {
        console.log("my-simple-btn: createdCallback");
    };

    proto.attachedCallback = function () {
        console.log("my-simple-btn: attachedCallback");
    };

    proto.detachedCallback = function () {
        console.log("my-simple-btn: detachedCallback");
    };

    proto.attributeChangedCallback = function (attrName, oldVal, newVal) {
        console.log("my-simple-btn: attributeChangedCallback", attrName, oldVal, newVal);
    };


    ce.MySimpleBtn = document.registerElement('my-simple-btn', {
        prototype: proto,
        extends: 'button'
    });


}(window.myCustomElements = (window.myCustomElements || {})));