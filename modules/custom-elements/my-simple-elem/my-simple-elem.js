(function (ce) {

    var proto = Object.create(HTMLElement.prototype);

    proto.createdCallback = function () {
        console.log("my-simple-elem: createdCallback");
    };

    proto.attachedCallback = function () {
        console.log("my-simple-elem: attachedCallback");
    };

    proto.detachedCallback = function () {
        console.log("my-simple-elem: detachedCallback");
    };

    proto.attributeChangedCallback = function (attrName, oldVal, newVal) {
        console.log("my-simple-elem: attributeChangedCallback", attrName, oldVal, newVal);
    };


    ce.MySimpleElem = document.registerElement('my-simple-elem', {
        prototype: proto
    });


}(window.myCustomElements = (window.myCustomElements || {})));