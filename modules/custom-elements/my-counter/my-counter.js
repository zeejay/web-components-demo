(function (ce) {

    var proto = Object.create(HTMLElement.prototype);

    proto.createdCallback = function() {

        var counter = this;

        counter.value = parseInt(counter.getAttribute("value") || 0, 10);

        var button = document.createElement("button");
        button.textContent = "Stop";
        button.onclick = function () {
            if (counter.interval) {
                counter.stop()
                button.textContent = "Resume";
            } else {
                button.textContent = "Stop";
                counter.resume();
            }
               
        };

        var span = document.createElement("span")
        span.className = "value-holder";
        span.style.marginLeft = "10px";
        span.style.marginRight = "10px";

        counter.appendChild(span);
        counter.appendChild(button);
    };

    proto.attachedCallback = function () {
        this.resume();
    };

    proto.resume = function () {
        var counter = this;
        counter.interval = setInterval(function (){
            counter.querySelector(".value-holder").innerHTML = counter.value++;
        }, 1000);
    };

    proto.stop = function () {
        var counter = this;
        if (counter.interval) {
            clearInterval(counter.interval);
            counter.interval = null;
        }
    };

    proto.getCurrentValue = function () {
        return this.value;
    };

        

    ce.MyCounter = document.registerElement('my-counter', {
        prototype: proto
    });

}(window.myCustomElements = (window.myCustomElements || {})));